GENERATED_PDFS=$(patsubst %.md,%.pdf,$(shell find documentation -name \*.md -print))
GENERATED_HTML= $(patsubst %.md, %.html, $(shell find documentation scenario-language . -maxdepth 1 -name \*.md -print))

CSS:="data:text/css;base64,QGltcG9ydCB1cmwoaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PUluY29uc29sYXRhfEludGVyJmRpc3BsYXk9c3dhcCk7Ym9keXttYXJnaW46NDBweCBhdXRvO21heC13aWR0aDo5MDBweDtsaW5lLWhlaWdodDoxLjY7Zm9udC1zaXplOjE2cHg7Y29sb3I6IzQ0NDtwYWRkaW5nOjAgMTBweDtmb250LWZhbWlseTpJbnRlcixzYW5zLXNlcmlmfWgxLGgyLGgze2xpbmUtaGVpZ2h0OjEuMjtmb250LWZhbWlseTpJbnRlcixzYW5zLXNlcmlmfWltZ3t3aWR0aDo3MDBweDtib3JkZXItcmFkaXVzOjEwcHh9cHJle2ZvbnQtZmFtaWx5OkluY29uc29sYXRhLG1vbm9zcGFjZX06OnNlbGVjdGlvbntjb2xvcjojZmZmO2JhY2tncm91bmQ6I2ZmNDA4MX0="

.PHONY: web
web: $(GENERATED_HTML)
	for i in $?; do mkdir -p public/$$(dirname $$i); mv -v $$i public/$$i; done
	mv -f public/README.html public/index.html

.PHONY: pdf
pdf: ${GENERATED_PDFS}

%.pdf: %.md
	@mkdir -p $(shell dirname $@)
	@pandoc -s --css=${CSS} -f markdown -t html $< -o $@ --metadata pagetitle="Gas Bug Bounty: $(shell basename $< | cut -d. -f1)"
	@echo "Wrote file://$(PWD)/$@"

%.html: %.md
	@mkdir -p $(shell dirname $@)
	@pandoc -s --css=${CSS} -f markdown -t html $< -o $@ --metadata pagetitle="Gas Bug Bounty: $(shell basename $< | cut -d. -f1)"
	@sed -i 's@href="\(.*\).md\(["#]\)@href="\1.html\2@g' $@
	@echo "Wrote file://$(PWD)/$@"

.PHONY: clean
clean:
	rm -f ${GENERATED_PDFS}

checklink: web
	checklink -r public/
