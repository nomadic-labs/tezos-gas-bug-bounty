let () =
  Printexc.register_printer @@ function
  | Parsing.Parse_error -> Some "parse error"
  | Failure message -> Some message
  | _ -> None

let parse_channel ch =
  let lexbuf = Lexing.from_channel ch in
  try Shell_parser.program Shell_lexer.(one_token (start ())) lexbuf
  with exn ->
    Printf.eprintf
      "%s:\n%s\n%!"
      (Shell_AST.show_loc (lexbuf.lex_start_p, lexbuf.lex_curr_p))
      (Printexc.to_string exn) ;
    exit 1

let () = parse_channel stdin |> Shell_AST.show |> print_endline
