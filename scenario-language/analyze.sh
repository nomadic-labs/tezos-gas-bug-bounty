#!/bin/sh

set -eu

. ./analyze.inc.sh

if [ $# -lt 1 ]; then
    echo "Usage: $0 [<log-dir> | <blocks-header-metadata.json> <main-node-events.json> <validator-node-events.json> ]"

    echo "Outputs in CSV for each block in <blocks-header-metadata.json> its level, "
    echo "block_hash, milligas, mgr_operation_application_time and validation_time "
    echo "and projected validation time."

    echo "Where <main-node-events.json> resp <validator-node-events.json> is the json, on-per-line log output of a main/validator node and "
    echo "<blocks-header-metadata.json> is the RPC output of /chains/main/blocks/, one "
    echo "line per block."

    exit 1
fi

if [ $# = 1 ]; then
    if [ ! -d "$1" ]; then
        echo "$1 is not an existing directory"
        exit 1
    fi

    blocks_header_metadata=$1/blocks_header_metadata.json
    main_node_events=$1/main-node-events.json
    validator_node_events=$1/validator-node-events.json
else
    blocks_header_metadata=$1
    main_node_events=$2
    validator_node_events=$3
fi

if [ ! -r "$blocks_header_metadata" ]; then
    echo "RPC results file $blocks_header_metadata is not readable"
    exit 1
fi

if [ ! -r "$main_node_events" ]; then
    echo "Node events file $main_node_events is not readable"
    exit 1
fi

if [ ! -r "$validator_node_events" ]; then
    echo "Node events file $validator_node_events is not readable"
    exit 1
fi

echo "level, block, milligas, projected_validation_ms, validation_ms (M), preapplication_ms (M), mgr_op_application_ms (S)"
jq -c < "$blocks_header_metadata" | while read -r block_header_metadata; do
    level=$(echo "$block_header_metadata" | jq .header.level)
    block=$(echo "$block_header_metadata" | jq .hash)

    # if [ "$block" != "null" ]; then
    #     echo "Warning: no block_hash at level $level" >&2


    milligas=$(echo "$block_header_metadata" | jq -r '.metadata.consumed_milligas')
    if [ "$milligas" = "null" ]; then
        echo "Warning: no milligas found at level $level" >&2
        milligas=""
        projected_validation_ms=""
    else
        # 1 milligas is 1 nanoseconds
        projected_validation_ms=$(units "$milligas nanoseconds" 'milliseconds' --terse --output-format "%.1f")
    fi

    if [ "$block" != "null" ]; then
        validation_time_s=$(validation_time_s_of_block_hash "$block" < "$main_node_events")
        validation_time_ms=$(units "$validation_time_s seconds" 'milliseconds' --terse --output-format "%.1f")
    else
        echo "Warning: no block_hash at level $level" >&2
        validation_time_ms=""
        block_hash=""
    fi

    preapplication_success_time_s=$(preapplication_success_time_s_of_level "$level" < "$main_node_events")
    if [ "$preapplication_success_time_s" != "null" ]; then
        preapplication_success_time_ms=$(units "$preapplication_success_time_s seconds" 'milliseconds' --terse --output-format "%.1f")
    else
        # echo "Warning: no preapplication_success_time_s at level $level" >&2
        preapplication_success_time_ms=""
    fi

    if [ -n "$block" ]; then
       # mgr_operation_application_time_s=$(mgr_operation_application_time_s_of_block "$block" < "$validator_node_events")
       mgr_operations_application_times_s=$(mgr_operations_application_times_s_of_block "$block" < "$validator_node_events")
       if [ -n "$mgr_operations_application_times_s" ] && [ "$mgr_operations_application_times_s" != "null" ]; then
           mgr_operations_application_times_ms=$(units "$mgr_operations_application_times_s seconds" 'milliseconds' --terse --output-format "%.1f")
       else
           # echo "Warning: no mgr_operation_application_time_s at level $level" >&2
           mgr_operations_application_times_ms=""
       fi
    else
        # echo "Warning: no mgr_operation_application_time_s at level $level" >&2
        mgr_operations_application_times_ms=""
    fi

    echo "$level, $block, $milligas, $projected_validation_ms, $validation_time_ms, $preapplication_success_time_ms, $mgr_operations_application_times_ms"
done
