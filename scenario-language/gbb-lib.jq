# Convert a string timestamp on the form
# "2022-05-13T10:19:48.143-00:00", as produced by the Octez node, to a
# number of seconds. Does not handle timezone, only use it to compare
# dates.
def datetimems_to_s:
  . | sub("-00:00$"; "") | split(".") | ((.[0] + "Z"|fromdate) + (.[1]|tonumber/1000));

# For a status object on the form {pushed: d1, treated: d2, completed: d3. }
# normalize each field w.r.t. the field pushed. I.e. return
# {pushed: 0, treated: d2-d1, completed: d3-d2. }
def event_status_relativize:
  . | map(datetimems_to_s) | { pushed: 0, treated: (.[1] - .[0]), completed: (.[2] - .[1]) }
;

# Unwrap event
def unsink:
  .["fd-sink-item.v0"]
;

# Select events from $section
def events_from_section($section):
  select(.|unsink|.section == ($section|split(".")))
;

# Select events from a worker with $worker_name
def events_from_worker($worker_name):
  select(.|unsink|.event|has($worker_name))
;

# Unwrap an worker event
def un_worker_event:
  .|unsink.event[][1]
;

# Retrieves completion_time for each validation events in a Octez node
# log.
#
# Example usage:
#
#  jq -L. 'include "gbb-lib"; validation_times_per_block' < node.json
def validation_times_per_block:
  . | events_from_section("validator.block")
  | events_from_worker("node_block_validator.v0")
  | un_worker_event
  | .event | select(has("successful_validation"))
  | {
      block: .successful_validation.block,
      completion_time: (.status | event_status_relativize | .completed)
  }
;

# Retrieves completion_time for each validation events in a Octez node
# log.
#
# Example usage:
#
#  jq -L. 'include "gbb-lib"; validation_times_per_block' < node.json

# {"fd-sink-item.v0":{"hostname":"arvid-XPS-13-9310","time_stamp":1654163325.774364,"section":["validation"],"event":{"applied_block.v0":{"block_hash":"BM9PhzorvswZcGPauCJwHotd6MD94swP3sR9NaYyVHfPBqxkAQP","operation_hash":"ooHACedEwHVHx7fyoWiHU8PRnF4CrPojuN18cEFpUWsShiqcyvx","application_time":0.0006425549999999999}}}}

def has_event($event_name):
  select(.|unsink|.event|has($event_name))
;

def get_event:
  .|unsink|.event[]
;

# Retrieves all applied_operations events
def operations_application_times_per_block:
  events_from_section("validation")
  | has_event("applied_operations.v0")
  | get_event
;

# Retrieves all .application_times for the applied_operations of a given block
def operations_application_times_of_block($block):
  operations_application_times_per_block
  | select(.block==$block)
  | .application_times
;


# Retrieves all sums the .application_times for the applied_operations
# corresponding to manager operations of a given block
def mgr_operations_application_times_of_block_sum($block):
  operations_application_times_of_block($block)
  | .[3] | map(.application_time) | add
;

# Retrieves all validator.block events and gives the relative timings
def block_validator_event_timings:
  . | events_from_section("validator.block")
  | select(.event|has("node_block_validator.v0")) | .event["node_block_validator.v0"]
  | [1].event
  | select(has("status"))
  | .status=(.status|event_status_relativize)
;

# Retrieves all validator.block events and gives the relative timings
def chain_validator_event_timings:
  .
  | events_from_section("validator.chain")
  | events_from_worker("node_chain_validator.v0")
  | un_worker_event
  | .event["processed_block"]
  | .status=(.status|event_status_relativize)
  | [.request.hash, .status.completed]
;
# TODO: compare ^ with pre-application times

# Select the level and completion times of each preapplication_success event
def preapplication_success_times_per_level:
  events_from_worker("node_block_validator.v0")
  | un_worker_event
  | .event
  | select(has("preapplication_success"))
  | { level: .preapplication_success.level|tonumber, completion_time: .status|event_status_relativize.completed}
;

# Select the completion time of all preapplication_success events of a
# node
def preapplication_success_times_of_level($level):
  preapplication_success_times_per_level | select(.level==$level) | .completion_time
;
