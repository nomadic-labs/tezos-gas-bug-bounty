type loc = Lexing.position * Lexing.position

type atom = {string : string; loc : loc}

type command = {executable : atom; arguments : atom list}

type seq_or_par = Seq | Par

type t = (seq_or_par * command) list

let show_command (seq_or_par, command) =
  let cmd =
    Filename.quote_command
      command.executable.string
      (List.map (fun x -> x.string) command.arguments)
  in
  match seq_or_par with Seq -> cmd | Par -> cmd ^ " &"

let show program = String.concat "\n" (List.map show_command program)

let show_loc ((a, b) : loc) =
  Printf.sprintf
    "File \"%s\", line %d, characters %d-%d"
    a.pos_fname
    a.pos_lnum
    (a.pos_cnum - a.pos_bol)
    (b.pos_cnum - a.pos_bol)
