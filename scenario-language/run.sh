#!/bin/sh

# shellcheck disable=SC3037


# double-node mode:

## start and connect two nodes (main and validator).
## the scenario interacts with the first node (main)
## wait until validator catches up
## the events are read from the second one (validator) for analysis

set -eu
if command -v shellcheck > /dev/null; then
    shellcheck -x "$0"
fi

run_dir=$(pwd)

. ./run.inc.sh

NODE_DIR=${NODE_DIR:-"$HOME/dev/nomadic-labs/tezos/master"}

start_node=0

cleanup () {
    _exit_code=$?
    set +e

    if [ "$_exit_code" != "0" ] && [ -n "${log_dir:-}" ]; then
        echo "Scenario execution interrupted, see logs in $log_dir";
    fi

    if [ -f LOG ]; then
        echo "Failure"
        echo
        cat LOG
        echo
        exit 1
    fi

    if [ "$start_node" = 1 ]; then
        pid_main=$(cat "/tmp/.node_pid_main")
        if [ -n "$pid_main" ] && ps -p "$pid_main" > /dev/null ; then
            info "Terminating main node ($pid_main)"
            kill "$pid_main"
        fi

        pid_validator=$(cat "/tmp/.node_pid_validator")
        if [ -n "$pid_validator" ] && ps -p "$pid_validator" > /dev/null; then
            info "Terminating validator node ($pid_validator)"
            kill "$pid_validator"
        fi
    fi

    exit $_exit_code
}
trap cleanup EXIT INT

silent () {
    if [ -n "${VERBOSE:-}" ]; then
        "$@"
    else
        "$@" > LOG 2>&1
        rm LOG
    fi
}

info () {
    echo "$@"
}

info_n () {
    echo -n "$@"
}

if [ ! -d "$NODE_DIR" ]; then
    echo "NODE_DIR is not an existing directory: $NODE_DIR"
    exit 1
fi

if [ $# -lt  1 ] || [ "$1" = "--help" ]; then
    cat <<EOT
Usage: $0 [--start-node] <scenario-file>

Runs the scenario file <scenario-file>, and then reports on
the gas usage and validation time for each produced block in
the scenario.

If '--start-node' is passed, then the binaries found in the directory pointed
to by the environment variable NODE_DIR is used to start a node.

Verbose mode can be toggled by setting the environment variable 'VERBOSE' to '1'.

Example:

   NODE_DIR=~/dev/tezos ./run.sh --start-node examples/id.sh
   VERBOSE=1 NODE_DIR=~/dev/tezos ./run.sh --start-node examples/noop.sh
EOT

    exit 1
fi

while [ $# -gt 0 ]; do
    if [ "$1" = "--start-node" ]; then
        start_node=1;
    else
        if [ -r "$1" ]; then
            scenario_file=$1
        else
            echo "Path to scenario file $1 is not a readable path"
            exit 1
        fi
    fi
    shift
done

log_dir="$(pwd)/.results/$(basename "$scenario_file")-$(date +%Y-%m-%d.%H%M%S)"
mkdir -p "$log_dir"
info "Results in: $log_dir"

NODE_ID_MAIN=1
NODE_ID_VALIDATOR=2

if [ "$start_node" = 1 ]; then
    info "Starting nodes in $NODE_DIR."

    cd "$NODE_DIR"
    # export TEZOS_EVENTS_CONFIG

    # Start main
    cd "$NODE_DIR/src/bin_node"

    info_n "Starting main node"
    {
        TEZOS_EVENTS_CONFIG="file-descriptor-path://${log_dir}/main-node-events.json?format=one-per-line" \
                           ./tezos-sandboxed-node.sh $NODE_ID_MAIN --connections 1 2>&1 &
        echo "$!" > "/tmp/.node_pid_main";
    } >"$log_dir"/main-node.log &
    sleep 2
    info " (pid $(cat /tmp/.node_pid_main)) with logs in $log_dir/main-node.log and $log_dir/main-node-events.json"

    info_n "Starting validator node"
    {
        # Enable debug information events from the validation section:
        # this is necessary to capture the 'applied_operations' event.
        TEZOS_EVENTS_CONFIG="file-descriptor-path://${log_dir}/validator-node-events.json?format=one-per-line&section-prefix=validation:debug&section-prefix=:info" \
                           ./tezos-sandboxed-node.sh $NODE_ID_VALIDATOR --connections 1 2>&1 &
        echo "$!" > "/tmp/.node_pid_";
    } > "$log_dir"/validator-node.log &
    sleep 2
    info " (pid $(cat /tmp/.node_pid_)) with logs in $log_dir/validator-node.log and $log_dir/validator-node-events.json"

    set +u
    cd "$NODE_DIR/src/bin_client" || exit

    # This only picks up the PATH I think
    ./tezos-init-sandboxed-client.sh $NODE_ID_MAIN > /tmp/sandbox_cmds 2>/dev/null
    grep -v trap /tmp/sandbox_cmds > /tmp/sandbox_cmds_no_trap
    # shellcheck disable=SC1091
    . /tmp/sandbox_cmds_no_trap
    set -u

    info "Connecting main to validator"
    silent tezos-admin-client trust address "127.0.0.1:1973${NODE_ID_VALIDATOR}"
    silent tezos-admin-client -E "http://127.0.0.1:1973${NODE_ID_VALIDATOR}" \
                       trust address "127.0.0.1:1973${NODE_ID_MAIN}"
    silent tezos-admin-client connect address "127.0.0.1:1973${NODE_ID_VALIDATOR}"

    info "Activating protocol Alpha"
    tezos-client -block genesis activate protocol ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK \
                 with fitness 1 and key activator and \
                 parameters "$NODE_DIR"/_build/default/src/proto_alpha/lib_parameters/sandbox-parameters.json

    cd "$run_dir" > /dev/null
else
    if ! command -v tezos-client > /dev/null; then
        echo "No tezos-client found, try passing '--start-node'"
        exit 1
    fi
fi

if ! command -v opam > /dev/null; then
    echo "No opam found, are you inside an OCaml switch?"
    exit 1
fi

info -n "Parsing scenario file $scenario_file: "
parse_log=$(mktemp)
if opam exec -- dune exec ./main.exe < "$scenario_file" > "$parse_log" 2>&1; then
    echo "OK"
else
    echo "Fail."
    echo "Log: "
    cat "$parse_log"
    exit 1
fi

start_head_level=$(rpc_head_level)


if [ -n "${VERBOSE:-}" ]; then
    echo "Debug information "
    echo "-----------------"
    echo "Log dir: $log_dir"
    echo "Client: $(command -v tezos-client)"
    echo "Client version: $(tezos-client --version)"
    echo "Node version:"
    tezos-client rpc get /version | jq
    echo "Node start level: ${start_head_level}"
fi

info "Running scenario $scenario_file with output in $log_dir/out.log"

start_time=$(date +%s.%N)

scenario_exitcode=0
if [ -n "${VERBOSE:-}" ]; then
    ( cd "$(dirname "$scenario_file")" && bash -ex "$(basename "$scenario_file")" 2>&1 ) \
        | tee "$log_dir/out.log" \
        || scenario_exitcode=$?
else
    ( cd "$(dirname "$scenario_file")" && bash -ex "$(basename "$scenario_file")" 2>&1 ) \
        > "$log_dir/out.log" \
        || scenario_exitcode=$?
fi
end_time=$(date +%s.%N)

end_head_level=$(rpc_head_level)

runtime=$(pp_seconds_float "$(echo "$end_time - $start_time" | bc)")

if [ $scenario_exitcode -eq 0 ]; then
  info -n "OK: "
else
  info -n "ERROR: "
fi
info "Scenario $scenario_file terminated with exit code $scenario_exitcode in $runtime."

validator_rpc_head_level() {
    tezos-client --endpoint "127.0.0.1:1873${NODE_ID_VALIDATOR}" \
                 rpc get chains/main/blocks/head/ \
        | jq -c '.header.level'
}

validator_rpc_level=$(validator_rpc_head_level)
info "Waiting for validator to catch up: $validator_rpc_level / $end_head_level"
while [ "$validator_rpc_level" -lt "$end_head_level" ]; do
    info "Waiting for validator to catch up: $validator_rpc_level / $end_head_level"
    sleep 1
    validator_rpc_level=$(validator_rpc_head_level)
done
info "Done"

if [ "$end_head_level" -gt "$start_head_level" ]; then
    echo -n "Dumping block metadata for blocks ($start_head_level .. $end_head_level): "
    touch "$log_dir"/blocks_header_metadata.json
    for level in $(seq $((start_head_level + 1)) 1 "$end_head_level"); do
        rpc_level_header_metadata "$level" >> "$log_dir"/blocks_header_metadata.json
        echo -n '.'
    done
    echo " Done!"

    prettify_csv() {
        if command -v csvlook > /dev/null; then csvlook --max-column-width 30; else cat; fi
    }

    analyzed_csv=$log_dir/results.csv
    ./analyze.sh "$log_dir" > "$analyzed_csv"
    echo "Report in $analyzed_csv: "
    prettify_csv < "$analyzed_csv"
else
    echo "No report: No blocks were produced, node remains at start head level $start_head_level"
fi
