%{
  open Shell_AST
%}

%token SEMI AMPERSAND EOF
%token <string> ATOM

%type <Shell_AST.t> program
%start program
%%

atom:
| ATOM
  { { string = $1; loc = Parsing.symbol_start_pos (), Parsing.symbol_end_pos () } }

arguments:
| atom arguments
  { $1 :: $2 }
|
  { [] }

command:
| atom arguments
  { { executable = $1; arguments = $2 } }

program:
| command SEMI program
  { (Seq, $1) :: $3 }
| command AMPERSAND program
  { (Par, $1) :: $3 }
| command EOF
  { [ Seq, $1 ] }
| SEMI program
  { $2 }
| EOF
  { [] }
