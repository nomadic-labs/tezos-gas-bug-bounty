#!/bin/sh

set -eu

if [ $# != 1 ]; then
    echo "Usage: $0 [<log-dir>]"

    cat <<EOT
Read a <log-dir> produced by 'run.sh' and use the results to find this
machine's nanoseconds per milligas using the least squares method.
EOT
    exit 1
fi

ns_per_mg=$(LC_NUMERIC=C awk -F, -f calibrate.awk < "$1/results.csv")
printf "This machine's nanoseconds per milligas as per %s/results.csv:\t" "${1%/}"
printf "%f\n" "$ns_per_mg"
