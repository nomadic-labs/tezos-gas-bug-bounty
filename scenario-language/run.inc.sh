#!/bin/sh

pp_seconds_float() {
    dt=$1

    dd=$(echo "$dt/86400" | bc)
    dt2=$(echo "$dt-86400*$dd" | bc)
    dh=$(echo "$dt2/3600" | bc)
    dt3=$(echo "$dt2-3600*$dh" | bc)
    dm=$(echo "$dt3/60" | bc)
    ds=$(echo "$dt3-60*$dm" | bc)
    LC_NUMERIC=C printf "%02d:%02d:%02.4f" "$dh" "$dm" "$ds"
}

rpc_head_level() {
    tezos-client rpc get chains/main/blocks/head/ \
        | jq -c '.header.level'
}

rpc_level_header_metadata() {
    level="$1"
    tezos-client rpc get chains/main/blocks/"$level"/
}
