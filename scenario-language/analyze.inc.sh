#!/bin/sh

validation_time_s_of_block_hash() {
    block_hash=$1

    jq -L. 'include "gbb-lib"; validation_times_per_block' \
        | jq -s 'map({key: .block, value: .completion_time}) | from_entries' \
        | jq -r ".[${block_hash}]"
}

preapplication_success_time_s_of_level() {
    level=$1

    jq  -L. --argjson level "$level" 'include "gbb-lib"; preapplication_success_times_of_level($level)' \
        | jq -s 'add'
}

operation_application_time_s_of_block() {
    block_hash=$1

    jq -L. --argjson block_hash "$block_hash" 'include "gbb-lib"; operation_application_time_of_block($block_hash)' \
        | jq -s 'add'
}

mgr_operation_application_time_s_of_block() {
    block_hash=$1

    jq -L. --argjson block_hash "$block_hash" 'include "gbb-lib"; mgr_operation_application_time_of_block($block_hash)' \
        | jq -s 'add'
}


mgr_operations_application_times_s_of_block() {
    block_hash=$1

    jq -L. --argjson block_hash "$block_hash" 'include "gbb-lib"; mgr_operations_application_times_of_block_sum($block_hash)'
}
