{
  open Shell_parser

  let end_of_atom atom =
    match atom with
      | [] ->
          []
      | _ :: _ ->
          [ ATOM (String.concat "" (List.rev atom)) ]
}

let bare_word = ['a'-'z' 'A'-'Z' '0'-'9' '_' '-' '+' '.' '/' ':' '=' '@' '^']+

rule token atom = parse
  | ('#' [^'\n']*)? '\n'
      {
        Lexing.new_line lexbuf;
        end_of_atom atom @ [ SEMI ]
      }
  | ('#' [^'\n']*)? eof
      { end_of_atom atom @ [ EOF ] }
  | [' ' '\t' '\r']+
      { end_of_atom atom }
  | '&'
      { end_of_atom atom @ [ AMPERSAND ] }
  | ';'
      { end_of_atom atom @ [ SEMI ] }
  | bare_word as x
      { token (x :: atom) lexbuf }
  | '\''
      { continue_single_quote atom lexbuf }
  | '"'
      { continue_double_quote atom lexbuf }
  | _ as char
      { failwith (Printf.sprintf "unsupported: %C" char) }

and continue_single_quote atom = parse
  | [^'\'' '\n']+ as x
      { continue_single_quote (x :: atom) lexbuf }
  | '\n'
      {
        Lexing.new_line lexbuf;
        continue_single_quote ("\n" :: atom) lexbuf
      }
  | '\''
      { token atom lexbuf }
  | eof
      { failwith "unterminated quote" }

and continue_double_quote atom = parse
  | [^'\"' '\\' '\n']+ as x
      { continue_double_quote (x :: atom) lexbuf }
  | '\n'
      {
        Lexing.new_line lexbuf;
        continue_double_quote ("\n" :: atom) lexbuf
      }
  | '\"'
      { token atom lexbuf }
  | '\\'
      { failwith "unsupported: escaped characters" }
  | eof
      { failwith "unterminated quote" }

{
  type state = Shell_parser.token list ref

  let start (): state = ref []

  let rec one_token (state: state) lexbuf =
    match !state with
      | head :: tail ->
          state := tail;
          head
      | [] ->
          state := token [] lexbuf;
          one_token state lexbuf
}
