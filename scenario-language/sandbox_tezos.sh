#! /bin/bash

shopt -s expand_aliases


default_tezos_dir="$HOME/dev/nomadic-labs/tezos/master"
tezos_dir=$(pwd)

while [[ ! -f $tezos_dir/tezos-client && ! $tezos_dir = "/" ]]; do
    tezos_dir=$(dirname "$tezos_dir")
done

if [[ ! -f $tezos_dir/tezos-client && -f "$default_tezos_dir/tezos-client" ]]; then
    tezos_dir=$default_tezos_dir
fi

if [[ ! -f $tezos_dir/tezos-client ]]; then
    echo "Could not find tezos-client, neither in $tezos_dir nor $default_tezos_dir";
    exit 1;
fi

echo "Running node from $tezos_dir"

# First look for running nodes

# Note: we use two greps so that the greps themselves do not show up in the list

nodes=$(ps aux | grep bin_node | grep main.exe | head -n 1)

if [[ $nodes ]]

then

    node_directory=$(echo "$nodes" | sed 's/.*--data-dir \([^ ]*\) .*/\1/')

    echo "A Tezos node is running (in $node_directory)."

else

    echo 'No Tezos node is running, launching a new one.'

    export BISECT_FILE=~/dev/nomadic-labs/tezos/_coverage_output/bisect
    # export TEZOS_EVENTS_CONFIG=file-descriptor-stdout://
    # cd "$tezos_dir/src/bin_node" &&  TEZOS_EVENTS_CONFIG=file-descriptor-stdout:// ./tezos-sandboxed-node.sh 1 --connections 1 &


    # note: all arguments are passed on to the node
    cd "$tezos_dir/src/bin_node" &&  ./tezos-sandboxed-node.sh 1 --connections 0 $@ 2>&1 | tee /tmp/sandbox_node.log &

    sleep 2

fi

cd "$tezos_dir/src/bin_client" || exit

eval $(opam env) # This is usefull to call the correct version of dune

./tezos-init-sandboxed-client.sh 1 > /tmp/sandbox_cmds

grep -v trap /tmp/sandbox_cmds > /tmp/sandbox_cmds_no_trap

. /tmp/sandbox_cmds_no_trap

# tezos-activate-alpha

if (tezos-client rpc get /chains/main/blocks/head/metadata | grep 'ALpha' &> /dev/null)

then

    echo "Protocol Alpha already activated"

else

    echo "Protocol Alpha not yet activated. Activating."

    tezos-activate-alpha

fi

# tezos-autocomplete

echo "alias tezos-client='$(which tezos-client)'" >> /tmp/sandbox_cmds
