# Gas Bug Bounty Scenario Language

The gas bug bounty allows reporters to attach bug scenarios that
demonstrate bugs by e.g. crafting blocks.

We here define the gas bug bounty scenario language and its
interpreter, whose implementation can be found
[here](https://gitlab.com/nomadic-labs/tezos-gas-bug-bounty/-/tree/main/scenario-language).

# Scenario Language

The scenario language is a subset of POSIX shell. It's grammar can be
found in `shell_parse.mly` in this folder.

A scenario consists of *commands*. A command is an *executable* and a
(potentially empty) list of *arguments*:

    tezos-client --wait none transfer 0 from bootstrap1 to bootstrap2

Commands can be executed in /sequence/, by separating them with
semi-colon or new lines. The following scenario calls `tezos-client`
thrice in sequence: first to inject a transfer, then baking a block
and finally baking a second block:

    tezos-client --wait none transfer 0 from bootstrap1 to bootstrap2
    tezos-client bake for bootstrap1; tezos-client bake for bootstrap1

Furthermore, commands can be launched in the background using `&`. In
the following scenario we launch a node in the background. In the
foregraound, we sleep for 10 seconds after which we use `tezos-client`
to call an RPC:

    tezos-node run &

    sleep 10

    tezos-client rpc get /chains/main/blocks/head

Finally, scenarios can be commented using `#`: all that follows until
the end of the line is ignored:

    # this is a comment
    sleep 10 # and this is too

## Parser

The file
[main.ml](https://gitlab.com/nomadic-labs/tezos-gas-bug-bounty/-/blob/main/scenario-language/main.ml)
implements a parser for this language.  It can be build in OCaml
1.14.0 with `dune` as the only prerequisite. Using
[opam](https://opam.ocaml.org/doc/Install.html):

```shell
git clone https://gitlab.com/nomadic-labs/tezos-gas-bug-bounty.git
cd tezos-gas-bug-bounty/scenario-language/
opam switch create . 1.14.0
opam install dune
dune build ./main.exe
```

Once built, the parser can be used to test whether a string parses as
an element in the scenario language:


```shell
dune exec ./main.exe < examples/consume.sh
```

The parser exits with error code zero if the string passed on standard
input parses.

## Proof-of-concept interpreter

A proof of concept interpreter is implemented as a bash script in
`run.sh`. It does not, per se, interpret the scenarios. Rather, it
pipes the scenarios to a bash shell. This works as the scenario
language is a strict subset of POSIX shell.

The scenarios run in a sandboxed Tezos network consisting of two
nodes: `main` and `validator`. The scenario interacts with and forges
operation using `main`. The blocks are gossiped to `validator`, where
validation time is measured.

(We use this two-node setup since various optimizations may impact the
time of application of manager operations and to not have
false-negative nor false-positive, it is better to measure the
application time of blocks received from the network).


### Dependencies and prerequisites

 - `jq`
 - (optional) `csvlook`
 - (optional) `shellcheck`

The interpreter requires a compiled Octez distribution on the local
computer. Please refer to [How to get
Tezos](https://tezos.gitlab.io/introduction/howtoget.html#setting-up-the-development-environment-from-scratch).

### Usage

To run the interpreter, refer to `./run.sh --help`:

```shell
Usage: ./run.sh [--start-node] <scenario-file>

Runs the scenario file <scenario-file>, and then reports on
the gas usage and validation time for each produced block in
the scenario.

If '--start-node' is passed, then the binaries found in the directory pointed
to by the environment variable NODE_DIR is used to start a node.

Verbose mode can be toggled by setting the environment variable 'VERBOSE' to '1'.

Example:

   NODE_DIR=~/dev/tezos ./run.sh --start-node examples/id.sh
   VERBOSE=1 NODE_DIR=~/dev/tezos ./run.sh --start-node examples/noop.sh
```

### Reading the output

Here's an example from running the scenario `examples/consume_more.sh`:

```
$ NODE_DIR=~/dev/nomadic-labs/tezos/arvid@gbb-block_validator-validation-times ./run.sh --start-node examples/consume_more.sh
Results in: /home/arvid/dev/nomadic-labs/gas-bug-bounty/scenario-language/.results/consume_more.sh-2022-06-16.110948
Starting nodes in /home/arvid/dev/nomadic-labs/tezos/arvid@gbb-block_validator-validation-times.
Starting main node (pid 1219318) with logs in /home/arvid/dev/nomadic-labs/gas-bug-bounty/scenario-language/.results/consume_more.sh-2022-06-16.110948/main-node.log and /home/arvid/dev/nomadic-labs/gas-bug-bounty/scenario-language/.results/consume_more.sh-2022-06-16.110948/main-node-events.json
Starting validator node (pid 1219349) with logs in /home/arvid/dev/nomadic-labs/gas-bug-bounty/scenario-language/.results/consume_more.sh-2022-06-16.110948/validator-node.log and /home/arvid/dev/nomadic-labs/gas-bug-bounty/scenario-language/.results/consume_more.sh-2022-06-16.110948/validator-node-events.json
Connecting main to validator
Activating protocol Alpha
Injected BLQGYLXGgxE7
```

First thing: a log directory is created in `.results`. This directory
will store all debug information and the resulting analysis of the
execution.

The `main` and `validator` node are started and connected to each
other. Protocol Alpha is injected in `main`. If you have a look in the
log directory, you'll find the following files at this point:

  - `main-node.log`, `validator-node.log`: the logs of the `main`
    respectively `validator` node.
  - `main-node-events.json`, `validator-node-events.json`: the events
    emitted by the `main` respectively `validator` node in JSON,
    one-event-per-line format.


Now scenario execution can start:

```
Parsing scenario file examples/consume_more.sh: OK
Running scenario examples/consume_more.sh with output in /home/arvid/dev/nomadic-labs/gas-bug-bounty/scenario-language/.results/consume_more.sh-2022-06-16.110948/out.log
OK: Scenario examples/consume_more.sh terminated with exit code 0 in 00:00:45.1939.

```

The scenario file is parsed, ensuring that it belongs to the shell
subset that is the scenario language. Finally, the scenario is
executed.

We now move on to the analysis phase:


```
Waiting for validator to catch up: 10 / 10
Done
Dumping block metadata for blocks (1 .. 10]: ......... Done!
Report in /home/arvid/dev/nomadic-labs/gas-bug-bounty/scenario-language/.results/consume_more.sh-2022-06-16.110948/results.csv: 
| level |  block                         |      milligas |  projected_validation_ms |  validation_ms (M) |  preapplication_ms (M) |  mgr_op_application_ms (S) |
| ----- | ------------------------------ | ------------- | ------------------------ | ------------------ | ---------------------- | -------------------------- |
|     2 |  "BLasyCuyxDsZWd9jwS6SWbJDY... |    12 605 000 |                     12,6 |                  7 |                     16 |                        2,9 |
|     3 |  "BMHTD9XqANa2gS45XWsNnd9fw... |     6 520 000 |                      6,5 |                  5 |                     15 |                        1,0 |
|     4 |  "BMA22TZNQrcxupvHCR95F5ZTG... |     6 565 000 |                      6,6 |                  4 |                     19 |                        1,1 |
|     5 |  "BM78vinrCMvZSdke4tSauWqmt... |     6 990 000 |                      7,0 |                  6 |                     21 |                        1,6 |
|     6 |  "BLSfE4WTK7szQ8BHLm4qMbTcP... |    11 265 000 |                     11,3 |                  3 |                     23 |                        8,1 |
|     7 |  "BMJoYTVAxK5nBtLtvKhcWtKh4... |    54 015 000 |                     54,0 |                  4 |                     60 |                       70,7 |
|     8 |  "BLdRy6ukskFEfvvnwTmzPgiKv... |   481 515 000 |                    481,5 |                  5 |                    471 |                      430,5 |
|     9 |  "BMSR6yEukywzpV9riT1MZGQzv... | 4 756 520 000 |                  4 756,5 |                  4 |                  8 860 |                    4 678,0 |
|    10 |  "BKxQA1WgixaYXmDYMxTwuwzJ4... |             0 |                      0,0 |                  3 |                      8 |                            |
```

First, we wait until the `validator` catches up to the `main` node. Then,
we use the RPC interface of the `main` node to retrieve the metadata of
the chain of blocks generated by the scenario (here `(1 .. 10)` means
that blocks 2 to 10 were generated by the scenario).

Finally, we collate the gas consumption of each block's metadata, with
the validation times observed in the `main` and the `validator` node
(retrieved from events emitted by the nodes) for the same blocks. The
result is in the table above and also in the file `results.csv` in the
log directory. The columns read (where columns labelled `(M)`
resp. `(S)` comes from the `main` resp. `validator` node):

   - `level`, `block`, `milligas`: the level, block hash and the
     milligas consumed by the block
   - `projected_validation_ms `: the projected validation time of the
     block in `ms`, which is obtained by dividing the consumed gas by
     $10^6$ (since one milligas is projected to execute in one
     nanosecond).
   - `validation_ms (M)`: (treated - completion
     time) of the `successful_validation` event of the
     `node_block_validator.v0` worker in the `validator.block` section
     (as emitted
     [here](https://gitlab.com/tezos/tezos/-/blob/0589a395d43e1b42397664c26b62a7a685e5738f/src/lib_shell/block_validator.ml#L393)).
   - `preapplication_ms (M)`: sum of the (treated - completion
     time) of the `preapplication_success` events of the
     `node_block_validator.v0` worker
     for this level
     (as emitted
     [here](https://gitlab.com/tezos/tezos/-/blob/0589a395d43e1b42397664c26b62a7a685e5738f/src/lib_shell/block_validator.ml#L408)).
   - `mgr_op_application_ms (S)`: sum of the `application_time` of the
     `applied_operations` events from the `validation` section for
     this level for manager operations (those in validation pass 3)
     (as emitted
     [here](https://gitlab.com/nomadic-labs/tezos/-/blob/dd2c82c99ef270feea5a5346d40b54445ced3193/src/lib_validation/block_validation.ml#L554)).
     This event is added in
     [tezos/tezos!5526](https://gitlab.com/tezos/tezos/-/merge_requests/5526/). If
     this column is missing, it means that you're running a node
     without this patch.


This `preapplication_ms` (resp. `mgr_op_application_ms`) column only
makes sense for the `main` (resp. `validator`) node, and is hence only
included for the that node. This is since `main` preapplies operations
from the operations from its mempool that are generated by the
scenario (emitting the preapplication event). This precludes the
emission of the `applied_operations` event.

Conversely, the validator receives full blocks over wire, and so does no
preapplication and instead emits the `applied_operations` event.

```
Terminating main node (1219318)
Terminating validator node (1219349)
```

Finally, the network is shut down.

# Example scenarios

We include a set of scenarios that are helpful for debugging and for illustration:

 - `examples/consume.sh`: runs the Michelson contract `examples/consume.tz` with an increasing argument. This contract consumes gas proportional to its argument.
 - `examples/consume_more.sh`:  as `consume.sh`, but calls `examples/consume.tz` multiple times per block.
 - `examples/example.sh`: illustrates the subset of shell supported by the scenario language
 - `examples/id.sh`: Originates and calls a simple contract `examples/id.tz`
 - `examples/noop.sh`: Does nothing
 - `examples/which_client.sh`: Calls `command -v tezos-client`. Useful for debugging

# Calibration

The gas model is fitted such that 1 milligas corresponds to 1 nanosecond of computation on the reference machine used to obtain the gas model.
It's worthwhile to consider how a given machine compares to the reference machine.

In particular, if a given machine is much more, or less, powerful than the reference machine, then the projected and measured validation time will differ.
However, that is not due to a gas bug.

This folder contains the script `calibration.sh` that computes the nanoseconds per milligas of a given scenario.

For instance, given the scenario execution above, we can use `calibration.sh` like this:

```shell
$ ./calibrate.sh .results/consume_more.sh-2022-06-16.110948/
This machine's nanoseconds per milligas as per .results/consume_more.sh-2022-06-16.110948/results.csv:	0.982615
```

That is, the machine that was used to run the `consume_more.sh`
scenario has a nanoseconds per milligas of ~0.98, which pretty close to
that of the reference machine (where it should be 1).

# Known issues

## Special characters in paths

The interpreter does not work when placed in a path that contains
spaces.
