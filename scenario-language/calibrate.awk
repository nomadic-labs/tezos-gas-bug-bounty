# The input file should be a results.csv as prodcued by run.sh, where:
#  - each line corresponds to one produced block
#  - column 3 (resp. 7)

# solve a = t * g
#
# where
## a = application time in the validator node, column 'mgr_op_application_ms (S)'
## t = the unknown, nanoseconds per milligas to solve for. in the reference machine, t = 1 ns/mg
## g = milligas consumed

# using least squares, ^t = sum(g[i] * a[i])/sum(g[i]^2)

BEGIN {
        denominator = 0.0;
        numerator = 0.0;
}
{
        if (NR > 1) {
                milligas = $3
                mgr_op_application_ms = $7
                denominator += milligas * (mgr_op_application_ms * 1000000);
                numerator += milligas * milligas
        }
}
END {
        print (denominator / numerator);
}
