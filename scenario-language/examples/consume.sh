#!/bin/sh

tezos-client --wait none originate contract consume transferring 0 from bootstrap1 running file:./consume.tz --burn-cap 10
sleep 0.1
tezos-client bake for

tezos-client --wait none transfer 0 from bootstrap1 to consume --arg '1'
sleep 0.1
tezos-client bake for

tezos-client --wait none transfer 0 from bootstrap1 to consume --arg '10'
sleep 0.1
tezos-client bake for

tezos-client --wait none transfer 0 from bootstrap1 to consume --arg '100'
sleep 0.1
tezos-client bake for

tezos-client --wait none transfer 0 from bootstrap1 to consume --arg '1000'
sleep 0.1
tezos-client bake for

tezos-client --wait none transfer 0 from bootstrap1 to consume --arg '10000'
sleep 0.1
tezos-client bake for

tezos-client --wait none transfer 0 from bootstrap1 to consume --arg '100000'
sleep 0.1
tezos-client bake for

tezos-client --wait none transfer 0 from bootstrap1 to consume --arg '1000000'
sleep 0.1
tezos-client bake for

tezos-client --wait none transfer 0 from bootstrap1 to consume --arg '10000000'
sleep 0.1
tezos-client bake for
