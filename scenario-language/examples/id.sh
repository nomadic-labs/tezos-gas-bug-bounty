#!/bin/sh

tezos-client --wait none originate contract id transferring 0 from bootstrap1 running file:./id.tz --burn-cap 10 --init '""'

sleep 1

tezos-client bake for

tezos-client --wait none transfer 0 from bootstrap1 to id --arg '"A test string"' --burn-cap 10

sleep 1

tezos-client bake for

sleep 1
