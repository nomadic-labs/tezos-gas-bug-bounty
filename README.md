# Tezos Gas Bug Bounty

Earn rewards by reporting bugs in Tezos' gas system!

See:

 - [Rules of the Game](documentation/rules.md) and [the bug report submission form](https://gas-bug-bounty.nomadic-labs.com):
   for a more general overview of the Gas Bug Bounty.

 - [Gas Bug Bounty: specification](documentation/specification.md):
   for a detailed specification of the Gas Bug Bounty.

 - [README for Gas Bug Reporters](documentation/README-reporters.md):
   for more detailed guidelines on how to report gas bugs.

 - [README for Gas Bug Validators](documentation/README-validators.md):
   for more detailed guidelines on the validation of gas bugs.

 - [README for the Infrastructure used](documentation/README-infrastructure.md):
   for more detailed guidelines on the infrastructure.

 - [README for the Scenario language](scenario-language/README.md):
   for more detailed description on the scenario language and its interpreter.

Good luck; have fun!