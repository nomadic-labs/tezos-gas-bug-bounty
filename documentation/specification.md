# Gas bug bounty: specification

# Stakeholders

-   Bug reporter: reports bugs
-   Bug validator(s): receives bug reports and runs bug scenario Charles and Arvid probab interprets results
    -   can be human (gas bug bounty crew) / computer
-   Legal team
-   Senior gas technician
    -   e.g. Lucas Randazzo
-   Incident responsible (*vigie* in French),
    -   e.g. Pierre or whoever is the incident responsible

# Version v0: PaPoC

## 1. Submission

-   A bug reporter submits a form at http://gas-bug-bounty.nomadic-labs.com/
    -   The form contains the definition (or a link to it), of a gas bug (see Appendix 0)
        -   Previous bugs should be defined as bugs by this definition
    -   The form contains the "rules of the game" (or a link to them), which details whether the submission will be eligible for the bounty.
    -   The form contains the fields:
        -   a description of the bug
        -   bug details:
            -   the protocol which is affected
            -   commit hash, if applicable
            -   line of code in file (GitLab link), if applicable
        -   email address of the reporter
        -   a field for attaching the bug scenario as a file
        -   a checkbox for pre-NDA

## 2. Prevalidation

-   The submission of a form results in an email sent to a dedicated address `gas-bug-bounty@nomadic-labs.com` or similar, containing the contents of the fields above and the attached scenario file.
    -   Optional: it should be quite easy to make it possible to submit the contents of the form to a dedicated, private slack channel `#gas-bug-bounty-reports`.
    -   Optional: we could create GitLab issues from the bug report

## 3. Validation

-   The bug validator will receive the email sent to `gas-bug-bounty@nomadic-labs.com`. They ensure the syntactical validity of the report by:
    -   making sure the description makes sense (to the best of their ability)
    -   verifying that the commit hash, line of code (GitLab link) make sense
    -   checking that the bug scenario is a syntactically valid program in the bug scenario language (See "Appendix 1 > Syntax" below)

-   If there are any syntactical issues in the report, the bug validator points out these to the reporter and invites them to resubmit (easier to automate) or to complete their report by email (friendlier).

-   If there are no syntactical issues in the report, the bug validator thanks the submitter and proceeds to bug validation.

-   The goal of bug validation is to verify if the report indeed falls under the definition of a gas bug as per Appendix 0
    -   The bug validator connects to the test machine

    -   They ensure that the test machine is in a clean state and that interference is minimal:
        -   no parasite processes are running
        -   no other gas bugs are being validated at the same time
        -   ...

    -   They launch a node running the affected protocol in the appropriate context (TODO: define appropriate context) and configuration (TODO: define appropriate configuration)
        -   Open question: if the node is supposed to run on a mainnet context, then only bugs on the mainnet protocol can be submitted.

        -   The logs of this node should be stored.

    -   They run the sequence of commands in the scenario
        -   If the scenario does not terminate in the `SCENARIO_TIMEOUT`, then the bug report is invalidated.

    -   After the execution of the script, processes launched by the process are terminated.

    -   The gas usage and validation time of each blocks in the nodes chain is compared as detailed in Appendix 0.

    -   This process is repeated `GAS_BUG_VALIDATION_REPEAT` times.

    -   If a gas bug is detected in `GAS_BUG_VALIDATION_THRESHOLD` of the `GAS_BUG_VALIDATION_REPEAT` cases, then a the bug is valid. If not, is it invalid.

    -   A validation report is generated. It contains for the `GAS_BUG_VALIDATION_REPEAT` runs:
        -   Whether the bug was validated or invalidated
        -   The total execution time of the scenario
        -   Output of each command
        -   The log of the node
        -   A summary of gas usage / validation time per block

### 4. Reporting

-   The validation report is sent to
    -   In all cases: gas-bug-bounty@nomadic-labs.com
    -   If valid:
        -   senior gas technician
        -   incident responsible
    -   Optional: the validation report can be sent to #gas-bug-bounty-reports slack channel
    -   Optional: the validation report can be attached to the GitLab issue corresponding to the bug report

-   An abbreviated the validation report is sent to the reporter. It should contain a subset of the full report, namely:
    -   Whether the bug was invalidated
        -   I think we should avoid the word "validated" even if the bug is not invalidated. We do not want to give the impression that we are promising a reward before the report has been processed by the senior gas technician and legal team
        -   If the bug is validated, we should rather say "thank you, we are looking into it and will contact you shortly"
    -   The total execution time of the scenario
    -   A summary of gas usage / validation time per block
    -   For security reasons, it should **not** contain:
        -   Output of each command
        -   The log of the node
    -   Should the reporter need to debug their scenario, they are referred to running the scenario locally or ask questions to the bug validator.

# Appendix 0: Definition of "Gas bug"



## Introduction: Gas and Validation Time

In tezos, a block consists of a sequence of operations. Tezos nodes execute the operations of a block against a context. This is also called validation. Validation results in an updated context. The validation of an operation (and by extension a block) consumes the artificial resource *gas*. The amount of gas consumed by validation can be retrieved through the RPC of a node.

Example: the gas consumed when validating block `N` is given by:

```shell
tezos-client rpc get chains/main/blocks/N | jq .metadata.consumed_milligas
```

Naturally, validating a block consumes other resources, notably time. The [wall time](https://en.wikipedia.org/wiki/Elapsed_real_time) necessary to validate a block, called the *validation time*, can be retrieved from the nodes logs::

```
1: Apr 29 15:06:56.011 - validator.block: block at level 2 successfully pre-applied
2: Apr 29 15:06:56.011 - validator.block: Request pushed on 2022-04-29T13:06:56.003-00:00, treated in 19.870us, completed in 7.109ms
3: Apr 29 15:06:56.021 - validator.block: block BLowp9ruyhPRgQfzBZVYbKGc7jhLZeM6ERPCgSwwQ2XwYHLqi9t successfully validated
4: Apr 29 15:06:56.021 - validator.block: Request pushed on 2022-04-29T13:06:56.012-00:00, treated in 12.236us, completed in 7.962ms
```

From the lines above, specifically on line 4, learn that the validation time for the block at level 2 with hash `BLowp9ruyhPRgQfzBZVYbKGc7jhLZeM6ERPCgSwwQ2XwYHLqi9t` is 7.962ms.

To goal of the artificial resource gas is to over-approximate the concrete resource validation time. More precisely, one milligas should correspond to 1 nanosecond of validation time on a specific "reference machine".

## Gas bugs

A gas bug occurs when the gas usage of a block is under-estimated or over-estimated[1].

By nature, the gas model is approximative. Consequently, gas bugs are too. For purpose of simplicity, we define a gas bug as:

-   a block whose gas usage `X milligas` is less/more than `Y%` of it's validation time `Z ns` on our test machine (see Appendix 2).

Furthermore, this under-/over-estimation must be confirmed statistically. It must be reproducible in `GAS_BUG_VALIDATION_TRESHOLD %` of `GAS_BUG_VALIDATION_REPEAT` cases.

TODO:

-   Define variables
-   Show how a reporter can try this at home : what is their ns per milligas on their machine?

# Appendix 1: Bug scenario language

TODO

## Syntax

TODO

## Semantics

TODO

# Appendix 2: Test machine specification

TODO

# Appendix 3: Test node specification

-   The node should be very much sandboxed network-wise.
-   The log output of the node should be such that block validation times are output (see Appendix 0 > Introduction) and preferably in a structured manner


# Footnotes

[1] Note that gas usage over-estimations are unfortunate, but do not pose security issues. On the other hand, severe gas underestimation can be exploited to craft operations with low gas usage but high validation time -- this can be used to block the Tezos network.
