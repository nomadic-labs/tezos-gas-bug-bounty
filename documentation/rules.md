# Tezos Gas Bug Bounty

## Expectations

In order to continuously improve the security of Tezos' gas system, the Tezos Foundation has set up a Gas Bug Bounty program. The policy below explains what security researchers and contributors can expect and the ground rules of the program.

Security researchers and contributors can expect the Tezos Foundation to: 

 - Review submissions relating to the improvement of the security of the Tezos' gas system,
 - Work with them to understand and validate their reports, provide timely initial responses to submissions,
 - Work to remediate discovered vulnerabilities in a reasonable timeframe,
 - Reward original contributions that are submitted in a complete, well-documented way.

## Scope

In scope of this policy are all bugs and errors found in the Tezos' gas system.

These include, but are not limited to, the following:

 1. Bugs in the Tezos protocol
 2. Bugs in Tezos shells (Octez, Tezedge)

For a detailed definition of the gas system we refer to the [Tezos developer documentation](https://tezos.gitlab.io/developer/michelson_instructions.html#the-gas-model), and for a description of bugs and errors in this system see [here](https://nomadic-labs.gitlab.io/tezos-gas-bug-bounty/documentation/tezos-gas-system-and-bugs.md#gas-bugs).

Should you have doubts as to the eligibility of the identified bug
please contact the [Tezos Foundation Security
Team](#questions).

## Bug Bounties

For confirmed vulnerabilities in Tezos' gas system, presented with a complete, well-documented report a bounty in Tezos tokens (“tez”) will be offered. The bounty will be proportional to the frequency of occurrence and/or the severity of the vulnerability being reported. The report should include a full description of the bug/exploit along with when and how it was discovered.

To ease reproducibility, the submission form allows you to upload a gas bug scenario script and accompanying files. The script, when executed, should interact with a Tezos node to produce a block that demonstrates the vulnerability. For more information about gas bug scenarios, see [here](https://nomadic-labs.gitlab.io/tezos-gas-bug-bounty/scenario-language/README.html).

### Ground Rules

To encourage vulnerability research, and to avoid any confusion between legitimate research and malicious attacks, we ask that security researchers attempt, in good faith, to abide by the following:

  - Play by the rules. This includes following this policy and any other commonly accepted best practices,
  - Promptly report any vulnerability discovered,
  - Avoid violating the privacy of others, disrupting systems, destroying data, and/or harming user experience,
  - Use only the channels defined in section below “How to report” to discuss vulnerability information with the Tezos Foundation,
  - Handle details of any vulnerability discovered confidentially,
  - Perform testing only on in-scope systems avoiding systems and activities which are out-of-scope,
  - Should a vulnerability provide unintended access to data:
    - Limit the amount of data accessed to the minimum necessary to demonstrate the vulnerability,
    - Cease testing and submit a report the moment user data is encountered including, but not limited to: Personally Identifiable Information (PII), Personal Healthcare Information (PHI), credit card data or proprietary information,
    - Only interact with test accounts one owns or with explicit permission from the account holder,
    - Do not engage in extortion.

### Exclusion

In order to establish a fair reward program, the following user groups are not eligible to receive bug bounties:

 - Tezos core development teams, Tezos Foundation employees and all other people paid directly or indirectly by the Tezos Foundation (i.e. subsidiaries, service providers, grantees…),
 - Any developer under contract with one of the Tezos organizations.

Furthermore, the following disqualifies the researcher from the Bug Bounty program:

 - Public disclosure of the vulnerability without coordination with the Tezos Foundation,
 - Submitting issues which have already been submitted by another security researcher or that already known to the Tezos Foundation. However, well-documented bugs submitted after having been previously submitted by other researchers lacking documentation, are acceptable.

### Legal Safe Harbor

For vulnerability research abiding by the conditions set out in this policy, the Tezos Foundation will consider said research to be:

 - Authorized with respect to any applicable computer crime legislation:
   - The Tezos Foundation will not initiate or support legal action against the security researcher for accidental, good-faith violations of this policy.
 - Authorized with respect to any relevant anti-reverse engineering legislation:
   - The Tezos Foundation will not initiate or support legal action against the security researcher for circumvention of anti-reverse engineering restrictions.
 - Exempt from restrictions in the Tezos Foundation’s policy which would interfere with conducting security research, waiving said restrictions on a limited basis.

Security researchers are expected to abide by all applicable laws. Should legal action be initiated by a third party against a security researcher, and the security researcher is in compliance with this policy, the Tezos Foundation will take appropriate steps to make it known that the security researcher’s actions were conducted in compliance with this policy.

Should a security researcher have concerns or doubts regarding the compliance of their research with this policy they are invited to contact the [Tezos Foundation Security Team](#questions).

## Reporting

Bug reports are submitted [here](https://gas-bug-bounty.nomadic-labs.com).

## Questions

Questions regarding the Tezos' Gas Bug Bounty can be sent via email to [gas-bug-bounty@nomadic-labs.com](mailto:gas-bug-bounty@nomadic-labs.com).
