# README for infrastructure used

## Context
To test a gas bug scenario, we need to run the bug interpreter. It will run some tezos nodes in our test machine and get the relevant metrics and produce the timing report.

## Terraform usage for AWS EC2 instance

In our environment, we will use an AWS EC2 m5d.2xlarge instance (the flavor can be chosen in the variables.tf file).

```
cd tf-aws-europe-basic-ec2
terraform init
terraform plan
terraform apply
```
```
cd tf-aws-europe-basic-ec2
terraform destroy
```

## AWS instance disk configuration
By default, the temp disk attached to the instance (nvme1n1) isn't formatted. In our case, our main user is `ubuntu`:
```
# Data disk formatting
sudo mkfs -t xfs /dev/nvme1n1
sudo mkdir /data
sudo mount /dev/nvme1n1 /data
sudo chown ubuntu:ubuntu /data
```

## Tezos node installation (can be skipped if already installed)

Based on the [official documentation](https://tezos.gitlab.io/introduction/howtoget.html#setting-up-the-development-environment-from-scratch):

```
cd /data

# Dependancies
sudo apt install -y rsync git m4 build-essential patch unzip wget pkg-config libgmp-dev libev-dev libhidapi-dev opam jq zlib1g-dev bc autoconf

# Rust installation
wget https://sh.rustup.rs/rustup-init.sh
chmod +x rustup-init.sh
./rustup-init.sh --profile minimal --default-toolchain 1.52.1 -y
source $HOME/.cargo/env

# ZCash Parameters
wget https://raw.githubusercontent.com/zcash/zcash/master/zcutil/fetch-params.sh
chmod +x fetch-params.sh
./fetch-params.sh

# Tezos
git clone https://gitlab.com/tezos/tezos.git
cd tezos
git checkout "latest-release"
opam init --bare
opam switch create . 4.12.1
OPAMSOLVERTIMEOUT=0 make build-deps
eval $(opam env)
make
```

## Building the test binary

```
sudo apt install -y units

cd /data
git clone https://gitlab.com/nomadic-labs/tezos-gas-bug-bounty.git
cd gas-bug-bounty/scenario-language/
opam init --bare
opam switch create . 4.12.1
eval $(opam env)
opam update
opam upgrade
opam install dune
dune build ./main.exe
```

## Running the test scenario

```
VERBOSE=1 NODE_DIR=/data/tezos/ ./run.sh --start-node examples/consume.sh
```
[For more information](https://nomadic-labs.gitlab.io/tezos-gas-bug-bounty/scenario-language/README.md).

## Get the calibration difference
```
./calibrate.sh scenario-language/.results/consume.sh-[DATE]
```
