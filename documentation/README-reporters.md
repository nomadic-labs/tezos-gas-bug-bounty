# README for Gas Bug Reporters

A practical guide to the Tezos Gas Bug Bounty.

## What is a gas bug?

See [here](https://nomadic-labs.gitlab.io/tezos-gas-bug-bounty/documentation/tezos-gas-system-and-bugs.html#gas-bugs).

## How are gas bugs validated?

Gas bugs are validated by reproducing the operation that consumes an
under/over-estimated amount of gas, and measuring the validation time of the
block on the reference machine.

To validate bugs locally before submission:

 1. create a reproduction script in the [scenario
    language](https://nomadic-labs.gitlab.io/tezos-gas-bug-bounty/scenario-language/README.md)
 2. compare your local machine to the gas model using the [calibration script](https://nomadic-labs.gitlab.io/tezos-gas-bug-bounty/scenario-language/README.md#calibration)
 3. use the scenario interpreter to run your script

## How are gas bugs submitted and how can I contact the bounty?

See the [rules](https://nomadic-labs.gitlab.io/tezos-gas-bug-bounty/documentation/rules.html), section [Reporting](https://nomadic-labs.gitlab.io/tezos-gas-bug-bounty/documentation/rules.html#reporting) and [Questions](https://nomadic-labs.gitlab.io/tezos-gas-bug-bounty/documentation/rules.html#questions).
