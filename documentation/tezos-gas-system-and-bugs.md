# Tezos' Gas system

Blocks in Tezos consists of a sequence of operations, such as smart contract calls. The nodes of the Tezos network execute the operations of each block. This is called validation. The validation of an operation (and by extension a block) consumes *gas*. Naturally, validating a block consumes other resources, notably time. The goal of *gas* is to over-estimate validation time. More precisely, each milligas consumed when validating a block is tuned to correspond to one nanosecond of validation time on a *gas reference machine*. For a more detailed description of Tezos' gas system, we refer to the [Tezos developer documentation](https://tezos.gitlab.io/developer/michelson_instructions.html#the-gas-model).

### Gas bugs

A gas bug occurs when the gas consumption of a block is *underestimated* or *overestimated* with respects to its actual validation time. The gas model is approximative and consequently, so are gas bugs. For purpose of simplicity, we define a gas bug as:

- a block whose validation time is more than 10x of it's gas consumption on the gas reference machine; or
- a block whose validation time is less than 0.1x of it's gas consumption on the gas reference machine.

Here are some examples:

|          | Gas consumption (milligas) | Validation time (ns) | Gas / time ratio | Gas bug? |
|----------|----------------------------|----------------------|------------------|----------|
| Block #1 | 3409                       | 80401                | 23.58x           | Yes      |
| Block #2 | 2423                       | 3444                 | 1.42x            | No       |
| Block #3 | 1000                       | 90                   | 0.09x            | Yes      |

The first block in this example consumes 3409 milligas. Consequently, its predicted validation time on the reference machine is 3409 ns. However, a validation time of 80401 ns was measured. Hence, the validation time is underestimated more than ten times: this is a gas bug. The validation time for the second block is also underestimated, but only by 1.42x. We consider such underestimations acceptable, and do not consider them as bugs. The third block is overestimated: the model would have it validated in 1000 ns, although in reality it only took 90 ns. This overestimation is also considered a bug.

Furthermore, the under/over-estimation must be confirmed statistically. It must be reproducible in at least 8 out of 10 test runs on the gas reference machine.
