# README for Gas Bug Validators

Brief guide to validating gas bug reports, to be completed.

Most of this information follows from reading the section [Validation in the specification](https://nomadic-labs.gitlab.io/tezos-gas-bug-bounty/documentation/specification.html).

Note that there is no validation machine: instead, use your own local machine after using the calibration script to [calibration script](https://nomadic-labs.gitlab.io/tezos-gas-bug-bounty/scenario-language/README.html#calibration) ensure it's performance does not differ wildly from that of the reference machine.

In the case a [scenario script](https://nomadic-labs.gitlab.io/tezos-gas-bug-bounty/scenario-language/README.html) is attached to the bug report: the steps that follows syntactical verification of the bug report is handled by the [scenario interpreter](https://nomadic-labs.gitlab.io/tezos-gas-bug-bounty/scenario-language/README.html).

If no scenario is attached, you will have to follow whatever indications are given by the reporter, if you deem the report otherwise credible (if unsure, escalate.)
